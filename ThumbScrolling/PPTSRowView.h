//
//  PPTSRowView.h
//  ThumbScrolling
//
//  Created by Pascal Pfiffner on 3/12/12.
//  Copyright (c) 2012 Pascal Pfiffner. All rights reserved.
//  This sourcecode is released under the Apache License, Version 2.0
//  http://www.apache.org/licenses/LICENSE-2.0.html/
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


/**
 *	A view representing one row
 */
@interface PPTSRowView : UIView {
	CGFloat borderWidth;
}

@property (nonatomic, copy) NSString *text;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, assign) CGRect textLocation;
@property (nonatomic, strong) NSIndexPath *indexPath;

@property (nonatomic, assign, getter=isActive) BOOL active;
@property (nonatomic, unsafe_unretained) PPTSRowView *prev;
@property (nonatomic, unsafe_unretained) PPTSRowView *next;

@property (nonatomic, assign) CGFloat targetY;

+ (id)rowWithCell:(UITableViewCell *)aCell;

- (void)setupWithCell:(UITableViewCell *)aCell reference:(PPTSRowView *)aReference;
- (void)showText:(NSString *)aString withFont:(UIFont *)aFont at:(CGRect)aRect;

- (void)foldWithFocusAt:(CGFloat)focusY inBubble:(CGFloat)bubbleSize withDensity:(CGFloat)density slowAnim:(BOOL)slow;
- (void)foldUpTowards:(CGRect)endFrame completion:(void (^)(BOOL finished))completion;

- (CGFloat)yForFocusY:(CGFloat)focusY inBubble:(CGFloat)bubbleSize withDensity:(CGFloat)density;
- (CATransform3D)transformForFocusY:(CGFloat)focusY inBubble:(CGFloat)bubbleSize withDensity:(CGFloat)density;
- (CGFloat)fractionForFocusY:(CGFloat)focusY withDensity:(CGFloat)density;


@end

CGFloat exponentialEaseInOut(CGFloat t);


#ifndef DLog
# ifdef DEBUG
#  define DLog(fmt, ...) NSLog((@"%s (line %d) " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
# else
#  define DLog(...)
# endif
#endif
