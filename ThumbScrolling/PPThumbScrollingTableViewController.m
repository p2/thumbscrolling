//
//  ThumbScrollingTableViewController.h
//  ThumbScrolling
//
//  Created by Pascal Pfiffner on 3/12/12.
//  Copyright (c) 2012 Pascal Pfiffner. All rights reserved.
//  This sourcecode is released under the Apache License, Version 2.0
//  http://www.apache.org/licenses/LICENSE-2.0.html/
//

#import "PPThumbScrollingTableViewController.h"
#import "PPTSView.h"
#import "PPTSRowView.h"


@interface PPThumbScrollingTableViewController ()

@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;
@property (nonatomic, strong) PPTSView *parent;

@property (nonatomic, assign) NSUInteger numRows;
@property (nonatomic, strong) NSMutableArray *numPerSection;				///< An array holding the number of items per section

@property (nonatomic, strong) NSIndexPath *activeIndexPath;					///< The indexPath that's supposed to be under the finger during scrolling
@property (nonatomic, strong) NSMutableArray *currentOrder;					///< An array holding the correct order of NSIndexPaths
@property (nonatomic, strong) NSMutableDictionary *currentCells;			///< A dictionary mapping index paths to cells

- (void)handleGesture:(UIGestureRecognizer *)aRecognizer;
- (void)moveThumbScrolling;

- (void)cacheCellsAroundFraction:(CGFloat)fraction;

@end


@implementation PPThumbScrollingTableViewController

@synthesize longPress, parent;
@synthesize numRows, numPerSection, activeIndexPath, currentOrder, currentCells;


#pragma mark - Setup
- (void)viewDidLoad
{
	[super viewDidLoad];
	
	if (self.tableView) {
		[self setup];
	}
	else {
		NSLog(@"viewDidLoad called without a table view in place!");
	}
}



#pragma mark - Setup
- (void)setup
{
	if (!longPress) {
		self.longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
		[self.tableView addGestureRecognizer:longPress];
	}
}


- (void)handleGesture:(UIGestureRecognizer *)aRecognizer;
{
	if (longPress == aRecognizer) {
		if (UIGestureRecognizerStateBegan == longPress.state) {
			[self startThumbScrolling:nil];
		}
		else if (UIGestureRecognizerStateChanged == longPress.state) {
			[self moveThumbScrolling];
		}
		else if (UIGestureRecognizerStateEnded == longPress.state) {
			[self endThumbScrolling];
		}
		else {
			DLog(@"State: %d", longPress.state);
		}
	}
	else {
		NSLog(@"Unknown recognizer: %@", aRecognizer);
	}
}



#pragma mark - Starting, Scrolling and Ending
- (void)startThumbScrolling:(id)sender
{
	NSUInteger i = 0;
	numRows = 0;
	self.numPerSection = [NSMutableArray array];
	self.currentOrder = [NSMutableArray arrayWithCapacity:kPPTSViewNumRows];
	self.currentCells = [NSMutableDictionary dictionaryWithCapacity:kPPTSViewNumRows];
	
	// count the rows per section
	for (; i < [self.tableView.dataSource numberOfSectionsInTableView:self.tableView]; i++) {
		NSUInteger num = [self.tableView.dataSource tableView:self.tableView numberOfRowsInSection:i];
		[numPerSection addObject:[NSNumber numberWithInt:num]];
		numRows += num;
	}
	
	// add the superview
	self.parent = [PPTSView newWithFrame:self.tableView.frame];
	parent.autoresizingMask = self.tableView.autoresizingMask;
	[[self.tableView superview] addSubview:parent];
	
	// determine starting point and cell
	CGPoint start = [longPress locationInView:self.tableView];
	CGFloat fraction = start.y / [self.tableView contentSize].height;
	CGPoint hitPoint = [parent convertPoint:start fromView:self.tableView];
	
	self.activeIndexPath = [self.tableView indexPathForRowAtPoint:start];
	NSArray *visibleCells = [self.tableView visibleCells];
	UITableViewCell *startCell = [self.tableView cellForRowAtIndexPath:activeIndexPath];
	
	// start
	if ([parent startWithVisibleCells:visibleCells activeCell:startCell activeIndex:activeIndexPath at:hitPoint.y asFraction:fraction of:numRows]) {
		[self cacheCellsAroundFraction:fraction];
		[parent moveTo:hitPoint withMapping:currentCells order:currentOrder active:nil isBeginning:YES];
	}
	else {
		DLog(@"Something failed");
		[parent removeFromSuperview];
		self.parent = nil;
	}
}


/**
 *	Moves the content according to the current location of the longPress gesture recognizer
 */
- (void)moveThumbScrolling
{
	if (!longPress) {
		DLog(@"Can't move, there is no long press recognizer in place!");
		return;
	}
	
	CGPoint hitPoint = [longPress locationInView:parent];
	CGFloat currY = hitPoint.y;
	CGFloat fraction = (currY - parent.offset) / [parent frame].size.height;
	
	[self cacheCellsAroundFraction:fraction];
	[parent moveTo:hitPoint withMapping:currentCells order:currentOrder active:activeIndexPath isBeginning:NO];
}


/**
 *	Ends thumb scrolling by flipping all rows back up and scrolling the table view to the represented position
 */
- (void)endThumbScrolling
{
	// scroll table to correct position
	CGPoint center = [parent centerOfActiveRow];
	NSIndexPath *end = [parent indexPathOfActiveRow];
	
	CGRect rowRect = [self.tableView rectForRowAtIndexPath:end];
	rowRect.origin.y -= center.y - (rowRect.size.height / 2);
	rowRect.size.height = [self.tableView bounds].size.height;
	[self.tableView scrollRectToVisible:rowRect animated:NO];
	
	// animate back into position
	[parent endInTableView:self.tableView animated:YES];
	
	// clean up
	self.parent = nil;
	self.numPerSection = nil;
	self.currentCells = nil;
}



#pragma mark - Retrieving data
- (void)cacheCellsAroundFraction:(CGFloat)fraction
{
	// determine offsets
	NSUInteger active = roundf(fraction * numRows);
	NSUInteger half = (kPPTSViewNumRows - 1) / 2;
	NSInteger low = active - half;
	NSInteger high = active + half;
	if (low < 0) {
		low = 0;
		high = kPPTSViewNumRows;
	}
	else if (high >= numRows) {
		low = numRows - kPPTSViewNumRows;
		high = numRows;
	}
	NSUInteger at = 0;
	
	// setup
	[currentOrder removeAllObjects];
	NSDictionary *existing = [currentCells copy];
	[currentCells removeAllObjects];
	self.activeIndexPath = nil;
	
	// get desired index paths
	NSUInteger section = 0;
	NSUInteger row = 0;
	for (NSNumber *numInSection in numPerSection) {
		row = 0;
		for (; row < [numInSection integerValue]; row++) {
			NSUInteger nowAt = at + row;
			if (nowAt >= low) {
				if (nowAt > high) {
					break;
				}
				
				// we want this!
				NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
				[currentOrder addObject:indexPath];
				
				UITableViewCell *cell = [existing objectForKey:indexPath];
				if (!cell) {
					cell = [self.tableView.dataSource tableView:self.tableView cellForRowAtIndexPath:indexPath];
				}
				
				// store it
				if (cell) {
					[currentCells setObject:cell forKey:indexPath];
					
					if (active == nowAt) {
						self.activeIndexPath = indexPath;
					}
				}
			}
		}
		at += row;
		section++;
	}
	
	if ([currentCells count] < 5) {
		DLog(@"low: %d, high: %d, total: %d, %d sections", low, high, numRows, [numPerSection count]);
	}
}


@end
