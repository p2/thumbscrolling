//
//  MasterViewController.h
//  ThumbScrolling
//
//  Created by Pascal Pfiffner on 3/12/12.
//  Copyright (c) 2012 Pascal Pfiffner. All rights reserved.
//  This sourcecode is released under the Apache License, Version 2.0
//  http://www.apache.org/licenses/LICENSE-2.0.html/
//

#import "PPThumbScrollingTableViewController.h"

@class DetailViewController;


/**
 *	The main name list table view controller
 */
@interface MasterViewController : PPThumbScrollingTableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end
