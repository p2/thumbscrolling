//
//  PPTSView.m
//  ThumbScrolling
//
//  Created by Pascal Pfiffner on 3/12/12.
//  Copyright (c) 2012 Pascal Pfiffner. All rights reserved.
//  This sourcecode is released under the Apache License, Version 2.0
//  http://www.apache.org/licenses/LICENSE-2.0.html/
//

#import "PPTSView.h"
#import "PPTSRowView.h"
#import <QuartzCore/QuartzCore.h>


@interface PPTSView ()

@property (nonatomic, readwrite, assign) CGFloat offset;			///< The offset of the start position from the mathematically correct position
@property (nonatomic, assign) CGFloat density;						///< How many pixels lie between one item and the next
@property (nonatomic, assign) CGRect rowFrame;						///< Standard row frame, used for newly created rows to get the correct dimensions

@property (nonatomic, strong) NSMutableArray *rowQueue;				///< Holds on to unused row objects

@property (nonatomic, strong) UIView *topFakeRows;					///< The view to mimic folded rows on the top
@property (nonatomic, strong) UIView *bottomFakeRows;					///< The view to mimic folded rows at the bottom

- (void)enqueueRow:(PPTSRowView *)aRow;
- (PPTSRowView *)dequeueRow;

@end


@implementation PPTSView

@synthesize bubbleSize;
@synthesize offset, density, rowFrame;
@synthesize rowQueue;
@synthesize topFakeRows, bottomFakeRows;


- (id)initWithFrame:(CGRect)aFrame
{
    if ((self = [super initWithFrame:aFrame])) {
		self.backgroundColor = [UIColor whiteColor];
		bubbleSize = 80.f;
		
		// fake box background
		CGRect bottomFrame = CGRectInset([self bounds], 30.f, 20.f);
		UIView *bottom = [[UIView alloc] initWithFrame:bottomFrame];
		bottom.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		bottom.layer.borderColor = [[UIColor lightGrayColor] CGColor];
		bottom.layer.borderWidth = 1.f;
		[self addSubview:bottom];
		
		// fake top rows
		self.topFakeRows = [[UIView alloc] initWithFrame:[self bounds]];
		topFakeRows.autoresizingMask = UIViewAutoresizingNone;
		topFakeRows.opaque = NO;
		topFakeRows.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_top.png"]];
		
		// fake bottom rows
		self.bottomFakeRows = [[UIView alloc] initWithFrame:[self bounds]];
		bottomFakeRows.autoresizingMask = UIViewAutoresizingNone;
		bottomFakeRows.opaque = NO;
		bottomFakeRows.backgroundColor = [UIColor clearColor];
		bottomFakeRows.clipsToBounds = YES;
		UIView *bgView = [[UIView alloc] initWithFrame:[bottomFakeRows bounds]];
		bgView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
		bgView.opaque = NO;
		bgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_bottom.png"]];
		[bottomFakeRows addSubview:bgView];
    }
    return self;
}

+ (id)newWithFrame:(CGRect)aFrame
{
	return [[self alloc] initWithFrame:aFrame];
}



#pragma mark - Initialization
/**
 *	Creates rows for the given visible cell array
 */
- (BOOL)startWithVisibleCells:(NSArray *)cellArray activeCell:(UITableViewCell *)activeCell activeIndex:(NSIndexPath *)activeIndexPath at:(CGFloat)startY asFraction:(CGFloat)fraction of:(NSUInteger)totalNum
{
	self.rowQueue = [NSMutableArray arrayWithCapacity:kPPTSViewNumRows];
	self.rowFrame = CGRectZero;
	
	// add top fake rows
	CGRect fakeFrame = [self bounds];
	fakeFrame.origin.y = -fakeFrame.size.height;
	topFakeRows.frame = fakeFrame;
	[self addSubview:topFakeRows];
	
	// add rows for all visible cells at their current position
	PPTSRowView *active = nil;
	PPTSRowView *prev = nil;
	for (UITableViewCell *cell in cellArray) {
		PPTSRowView *row = [PPTSRowView rowWithCell:cell];
		row.frame = [self convertRect:cell.frame fromView:cell.superview];
		if (rowFrame.size.width < 1.f) {
			self.rowFrame = row.frame;
		}
		
		[self addSubview:row];
		
		// active starting cell?
		if (cell == activeCell) {
			row.active = YES;
			row.indexPath = activeIndexPath;
			row.targetY = startY;
			active = row;
		}
		
		row.prev = prev;
		prev = row;
	}
	
	// determine offset from where we should be
	CGFloat myHeight = [self bounds].size.height;
	CGFloat wantY = myHeight * fraction;
	offset = startY - wantY;
	self.density = myHeight / totalNum;
	//DLog(@"num: %d, fraction: %f, density: %f, offset: %f", totalNum, fraction, density, offset);
	
	// set target position for everybody
	prev = active;
	NSUInteger dist = 1;
	while ((prev = prev.prev)) {
		prev.targetY = startY - dist * density;
		dist++;
	}
	prev = active;
	dist = 1;
	while ((prev = prev.next)) {
		prev.targetY = startY + dist * density;
		dist++;
	}
	
	// add bottom fake rows
	fakeFrame = [self bounds];
	fakeFrame.origin.y = fakeFrame.size.height;
	fakeFrame.size.height = 3.f;					// don't go to zero to prevent issues (3px is the height of the bottom pattern image)
	bottomFakeRows.frame = fakeFrame;
	[self addSubview:bottomFakeRows];
	
	return YES;
}



#pragma mark - Moving
/**
 *	Move focus to the given point
 *	@param hitPoint Current finger position that should be the focus point
 *	@param mapping Dictionary with indexPath => cell mappings for all visible rows
 *	@param order Array with indexPaths for the order of the "mapping" dictionary
 *	@param activeIndexPath Current rows will be searched for this and the one matching this index path will become the active row. If activeIndexPath
 *		is nil, the row which responds YES to isActive will be taken as the active one.
 *	@param beginning If YES, some animations will be slower, used when starting the scrolling
 */
- (void)moveTo:(CGPoint)hitPoint withMapping:(NSDictionary *)mapping order:(NSArray *)order active:(NSIndexPath *)activeIndexPath isBeginning:(BOOL)beginning
{
	CGFloat newY = hitPoint.y;
	
	// past the offset?
	if (newY < offset) {
		DLog(@"PAST UP ABOVE");
		/// @todo handle
		return;
	}
	
	// determine which row is the active row by comparing it to the active index path
	PPTSRowView *active = nil;
	for (PPTSRowView *row in [self subviews]) {
		if ([row isKindOfClass:[PPTSRowView class]]) {
			if ((activeIndexPath && [row.indexPath isEqual:activeIndexPath]) || (!activeIndexPath && row.active)) {
				row.active = YES;
				active = row;
			}
			else {
				row.active = NO;
			}
		}
	}
	
	// no active row, enqueue everybody (because we don't have a chain link) and dequeue an active one!
	if (!active) {
		for (PPTSRowView *row in [self subviews]) {
			if ([row isKindOfClass:[PPTSRowView class]]) {
				[self enqueueRow:row];
			}
		}
		
		active = [self dequeueRow];
		active.active = YES;
		active.indexPath = activeIndexPath;
		
		active.frame = rowFrame;
		active.targetY = newY;			/// @todo use density and offset to determine the correct target location
		active.center = CGPointMake(rowFrame.size.width / 2, newY);
		active.layer.transform = [active transformForFocusY:newY inBubble:bubbleSize withDensity:density];
		
		[self insertSubview:active belowSubview:bottomFakeRows];
	}
	[active foldWithFocusAt:newY inBubble:bubbleSize withDensity:density slowAnim:beginning];
	
	
	// update lower cell values
	CGFloat refY = active.targetY;
	PPTSRowView *lastValid = active;
	PPTSRowView *next = active.next;
	
	NSInteger activeIndex = -1;
	NSInteger at = 0;
	NSUInteger dist = 1;				///< distance from active
	for (NSIndexPath *indexPath in order) {
		if (activeIndex < 0 && [active.indexPath isEqual:indexPath]) {
			activeIndex = at;
		}
		
		
		// *** later cells
		else if (activeIndex >= 0) {
			
			// need a new "next" cell!
			if (!next) {
				next = [self dequeueRow];
				next.frame = rowFrame;
				next.targetY = refY + round(dist * density);
				if (beginning) {
					next.center = CGPointMake(rowFrame.size.width / 2, active.center.y + dist * rowFrame.size.height);
				}
				else {
					next.center = CGPointMake(rowFrame.size.width / 2, [next yForFocusY:newY inBubble:bubbleSize withDensity:density]);
					next.layer.transform = [next transformForFocusY:newY inBubble:bubbleSize withDensity:density];
				}
				next.prev = lastValid;
				
				[self insertSubview:next aboveSubview:next.prev];
			}
			
			// update
			if (![next.indexPath isEqual:indexPath]) {
				next.indexPath = indexPath;
				[next setupWithCell:[mapping objectForKey:indexPath] reference:active];
			}
			[next foldWithFocusAt:newY inBubble:bubbleSize withDensity:density slowAnim:beginning];
			
			lastValid = next;
			next = next.next;
			dist++;
		}
		at++;
	}
	
	// still more cells?
	while (next) {
		PPTSRowView *nextNext = next.next;
		[self enqueueRow:next];
		next = nextNext;
	}
	
	
	// *** earlier cells
	lastValid = active;
	PPTSRowView *previous = active.prev;
	
	at = [order count] - 1;
	dist = 1;
	for (NSIndexPath *indexPath in [order reverseObjectEnumerator]) {
		if (at < activeIndex) {
			
			// need a new "previous" cell!
			if (!previous) {
				previous = [self dequeueRow];
				previous.frame = rowFrame;
				previous.targetY = refY - round(dist * density);
				if (beginning) {
					previous.center = CGPointMake(rowFrame.size.width / 2, active.center.y - dist * rowFrame.size.height);
				}
				else {
					previous.center = CGPointMake(rowFrame.size.width / 2, [previous yForFocusY:newY inBubble:bubbleSize withDensity:density]);
					previous.layer.transform = [previous transformForFocusY:newY inBubble:bubbleSize withDensity:density];
				}
				previous.next = lastValid;
				
				[self insertSubview:previous belowSubview:previous.next];
			}
			
			// update
			if (![previous.indexPath isEqual:indexPath]) {
				previous.indexPath = indexPath;
				[previous setupWithCell:[mapping objectForKey:indexPath] reference:active];
			}
			[previous foldWithFocusAt:newY inBubble:bubbleSize withDensity:density slowAnim:beginning];
			
			lastValid = previous;
			previous = previous.prev;
			dist++;
		}
		at--;
	}
	
	// some more atop?
	while (previous) {
		PPTSRowView *prevPrev = previous.prev;
		[self enqueueRow:previous];
		previous = prevPrev;
	}
	
	// update fake rows
	CGRect topFrame = topFakeRows.frame;
	topFrame.origin.y = 2.f;
	topFrame.size.height = hitPoint.y - bubbleSize/3*2 - 6.f;		// 6: manual correction
	
	CGRect botFrame = [self bounds];
	botFrame.origin.y = hitPoint.y + bubbleSize/2 - 6.f;			// 6: manual correction
	botFrame.size.height = botFrame.size.height - botFrame.origin.y - 2.f;
	
	if (beginning) {		// using 0.0 here if NOT beginning leaves artifacts compared to not assigning the frames in the animation block
		[UIView animateWithDuration:0.3
						 animations:^{
							 topFakeRows.frame = topFrame;
							 bottomFakeRows.frame = botFrame;
						 }];
	}
	else {
		topFakeRows.frame = topFrame;
		bottomFakeRows.frame = botFrame;
	}
}



#pragma mark - Row Handling
/**
 *	Removes the row from the view and enqueues it for later use
 *	@todo Implement
 */
- (void)enqueueRow:(PPTSRowView *)aRow
{
	if (aRow) {
		aRow.prev = nil;
		aRow.next = nil;
		aRow.indexPath = nil;
		[rowQueue addObject:aRow];
		[aRow removeFromSuperview];
	}
}

/**
 *	Returns a row, either dequeued or newly allocated
 *	@todo Implement
 */
- (PPTSRowView *)dequeueRow;
{
	PPTSRowView *row = [rowQueue lastObject];
	if (row) {
		[rowQueue removeLastObject];
	}
	else {
		row = [[PPTSRowView alloc] initWithFrame:CGRectMake(0.f, 0.f, 320.f, 44.f)];		// the frame will be changed immediately
	}
	return row;
}

- (CGPoint)centerOfActiveRow
{
	for (PPTSRowView *row in [self subviews]) {
		if ([row isKindOfClass:[PPTSRowView class]]) {
			if (row.isActive) {
				return row.center;
			}
		}
	}
	
	return CGPointZero;
}

- (NSIndexPath *)indexPathOfActiveRow
{
	for (PPTSRowView *row in [self subviews]) {
		if ([row isKindOfClass:[PPTSRowView class]]) {
			if (row.isActive) {
				return row.indexPath;
			}
		}
	}
	
	return nil;
}



#pragma mark - Ending
/**
 *	Makes all rows fold up and returns the index path of the currently active row
 */
- (NSIndexPath *)endInTableView:(UITableView *)aTableView animated:(BOOL)animated
{
	id callback = ^(BOOL finished) {
		self.rowQueue = nil;
		[self removeFromSuperview];			/// @todo find a better solution, we must not remove ourselves from superview before the following loop is through
	};
	
	// have all rows fold up
	NSIndexPath *index = nil;
	for (PPTSRowView *row in [self subviews]) {
		if ([row isKindOfClass:[PPTSRowView class]]) {
			CGRect target = [self convertRect:[aTableView rectForRowAtIndexPath:row.indexPath] fromView:aTableView];
			[row foldUpTowards:target completion:callback];
			callback = nil;
			
			if (row.isActive) {
				index = row.indexPath;
			}
		}
	}
	
	// slide fake rows off
	CGRect topFrame = topFakeRows.frame;
	topFrame.origin.y = -topFrame.size.height;
	CGRect botFrame = bottomFakeRows.frame;
	botFrame.origin.y = [self bounds].size.height;
	botFrame.size.height = 0.f;
	
	[UIView animateWithDuration:(animated ? 0.2 : 0.0)
					 animations:^{
						 topFakeRows.frame = topFrame;
						 bottomFakeRows.frame = botFrame;
					 }
					 completion:^(BOOL finished){
						 [topFakeRows removeFromSuperview];
						 [bottomFakeRows removeFromSuperview];
					 }];
	
	return index;
}


@end
