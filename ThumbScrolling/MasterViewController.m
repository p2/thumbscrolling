//
//  MasterViewController.m
//  ThumbScrolling
//
//  Created by Pascal Pfiffner on 3/12/12.
//  Copyright (c) 2012 Pascal Pfiffner. All rights reserved.
//  This sourcecode is released under the Apache License, Version 2.0
//  http://www.apache.org/licenses/LICENSE-2.0.html/
//

#import "MasterViewController.h"
#import "DetailViewController.h"


@interface MasterViewController () {
    NSMutableArray *_objects;
	NSMutableArray *_titles;
}

@property (nonatomic, copy) NSArray *all;

- (void)loadNames;

@end


@implementation MasterViewController

@synthesize all;


@synthesize detailViewController = _detailViewController;


- (void)viewDidLoad
{
    [super viewDidLoad];
	//self.navigationItem.leftBarButtonItem = self.editButtonItem;
	//UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
	//self.navigationItem.rightBarButtonItem = addButton;

	self.tableView.sectionIndexMinimumDisplayRowCount = 20;
	
	// the switch
	NSArray *nums = [NSArray arrayWithObjects:@"100", @"250", @"1000", nil];
	UISegmentedControl *selector = [[UISegmentedControl alloc] initWithItems:nums];
	selector.segmentedControlStyle = UISegmentedControlStyleBar;
	selector.selectedSegmentIndex = 0;
	[selector addTarget:self action:@selector(loadNames) forControlEvents:UIControlEventValueChanged];
	self.navigationItem.titleView = selector;
	
	// start
	[self loadNames];
}

- (void)loadNames
{
	// read names file
	NSString *path = [[NSBundle mainBundle] pathForResource:@"dist.male.first" ofType:@"txt"];
	NSString *fullString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
	
	NSArray *foo = [fullString componentsSeparatedByString:@"\n"];
	self.all = [foo sortedArrayUsingSelector:@selector(compare:)];
	
	// fill objects and titles
	_objects = [NSMutableArray new];
	_titles = [NSMutableArray new];
	NSUInteger i = 0;
	NSUInteger used = 0;
	
	NSUInteger selectedIndex = ((UISegmentedControl *)self.navigationItem.titleView).selectedSegmentIndex;			// bad  bad bad, don't do this. But I'm lazy.
	NSUInteger every = (0 == selectedIndex) ? 10 : ((1 == selectedIndex) ? 4 : 1);
	NSMutableString *currentFirst = [NSMutableString string];
	NSMutableArray *section = [NSMutableArray arrayWithCapacity:50];
	
	for (NSString *line in all) {
		if (0 == i % every) {
			NSArray *parts = [line componentsSeparatedByString:@" "];
			if ([parts count] > 0) {
				NSString *name = [parts objectAtIndex:0];
				if ([name length] > 0) {
					NSString *first = [name substringToIndex:1];
					
					// add section
					if (![first isEqualToString:currentFirst]) {
						[_titles addObject:[currentFirst copy]];
						[_objects addObject:[section copy]];
						[section removeAllObjects];
						[currentFirst setString:first];
					}
					
					// add object
					[section addObject:name];
					used++;
				}
			}
		}
		i++;
	}
	[_titles addObject:[currentFirst copy]];
	[_objects addObject:[section copy]];
	
	self.title = [NSString stringWithFormat:@"%d Names", used];
	
	[self.tableView reloadData];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:@"New" atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}



#pragma mark - Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [_objects count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if ([_objects count] > section && [[_objects objectAtIndex:section] count] > 0) {
		NSString *any = [[_objects objectAtIndex:section] objectAtIndex:0];
		if ([any length] > 0) {
			return [any substringToIndex:1];
		}
	}
	return nil;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
	return _titles;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if ([_objects count] > section) {
		return [[_objects objectAtIndex:section] count];
	}
	return 0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

	if ([_objects count] > indexPath.section) {
		NSString *name = [[_objects objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
		cell.textLabel.text = name;
	}
	else {
		cell.textLabel.text = @"Overflow";
	}
    return cell;
}



#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.detailViewController) {
        self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    }
	
	if ([_objects count] > indexPath.section) {
		NSString *name = [[_objects objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
		self.detailViewController.detailItem = name;
		[self.navigationController pushViewController:self.detailViewController animated:YES];
	}
}


@end
