//
//  PPTSView.h
//  ThumbScrolling
//
//  Created by Pascal Pfiffner on 3/12/12.
//  Copyright (c) 2012 Pascal Pfiffner. All rights reserved.
//  This sourcecode is released under the Apache License, Version 2.0
//  http://www.apache.org/licenses/LICENSE-2.0.html/
//

#import <UIKit/UIKit.h>

#define kPPTSViewNumRows 11


@interface PPTSView : UIView

@property (nonatomic, assign) CGFloat bubbleSize;				///< 40.f by default
@property (nonatomic, readonly, assign) CGFloat offset;			///< The difference to the actual, scaled point in our table view

+ (id)newWithFrame:(CGRect)aFrame;

- (BOOL)startWithVisibleCells:(NSArray *)cellArray activeCell:(UITableViewCell *)activeCell activeIndex:(NSIndexPath *)activeIndexPath at:(CGFloat)startY asFraction:(CGFloat)fraction of:(NSUInteger)totalNum;
- (void)moveTo:(CGPoint)hitPoint withMapping:(NSDictionary *)mapping order:(NSArray *)order active:(NSIndexPath *)activeIndexPath isBeginning:(BOOL)beginning;
- (NSIndexPath *)endInTableView:(UITableView *)aTableView animated:(BOOL)animated;

- (CGPoint)centerOfActiveRow;
- (NSIndexPath *)indexPathOfActiveRow;


@end
