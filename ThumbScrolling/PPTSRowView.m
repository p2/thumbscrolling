//
//  PPTSRowView.m
//  ThumbScrolling
//
//  Created by Pascal Pfiffner on 3/12/12.
//  Copyright (c) 2012 Pascal Pfiffner. All rights reserved.
//  This sourcecode is released under the Apache License, Version 2.0
//  http://www.apache.org/licenses/LICENSE-2.0.html/
//

#import "PPTSRowView.h"


@implementation PPTSRowView

@synthesize text, font, textLocation, indexPath;
@synthesize active, prev, next, targetY;


- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
		borderWidth = 1.f;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

+ (id)rowWithCell:(UITableViewCell *)aCell
{
	PPTSRowView *row = [[self alloc] initWithFrame:aCell.frame];
	[row setupWithCell:aCell reference:nil];
	
	return row;
}



#pragma mark - Setup
/**
 *	Tries to copy the text from the cell's textLabel. Because the cell is not shown in the table view, the metrics are off, which is why we
 *	use a reference row if needed
 */
- (void)setupWithCell:(UITableViewCell *)aCell reference:(PPTSRowView *)aReference
{
	UILabel *cellLabel = aCell.textLabel;
	
	// check the font
	UIFont *aFont = cellLabel.font;
	if (aFont.pointSize < 4.f) {
		aFont = aReference.font;
	}
	
	// check the text label frame
	CGRect cellFrame = cellLabel.frame;
	if (cellFrame.size.width > 0.f && cellFrame.size.height > 0.f) {
		cellFrame = [aCell convertRect:cellFrame fromView:cellLabel.superview];
		cellFrame.origin.y += 10.f;			// why is this incorrect to begin with??
	}
	else {
		cellFrame = aReference.textLocation;
	}
	
	[self showText:cellLabel.text withFont:aFont at:cellFrame];
}

- (void)showText:(NSString *)aString withFont:(UIFont *)aFont at:(CGRect)aRect
{
	//DLog(@"%@ in %@ at %@", aString, aFont, NSStringFromCGRect(aRect));
	self.text = aString;
	
	// font
	if (aFont && aFont.pointSize > 0.f) {
		self.font = aFont;
	}
	else if (!font) {
		self.font = [UIFont boldSystemFontOfSize:19.f];
	}
	
	// text area
	if (aRect.size.width > 0.f && aRect.size.height > 0.f) {
		self.textLocation = aRect;
	}
	
	[self setNeedsDisplay];
}



#pragma mark - Animation
/**
 *	Folds down the receiver to align to y as focal point
 */
- (void)foldWithFocusAt:(CGFloat)focusY inBubble:(CGFloat)bubbleSize withDensity:(CGFloat)density slowAnim:(BOOL)slow
{
	CGFloat y = [self yForFocusY:focusY inBubble:bubbleSize withDensity:density];
	CATransform3D t = [self transformForFocusY:focusY inBubble:bubbleSize withDensity:density];
	//borderWidth = 2.f - (1.f * [self fractionForFocusY:focusY withDensity:density]);
	//[self setNeedsDisplay];
	
	// animate transformation
	[UIView animateWithDuration:(slow ? 0.3 : 0.05)
						  delay:0.0
						options:UIViewAnimationOptionBeginFromCurrentState
					 animations:^{
						 self.center = CGPointMake(self.center.x, y);
						 self.layer.transform = t;
					 }
					 completion:NULL];
}


- (void)foldUpTowards:(CGRect)endFrame completion:(void (^)(BOOL finished))completion
{
	[UIView animateWithDuration:0.2
					 animations:^{
						 self.frame = endFrame;
						 self.layer.transform = CATransform3DIdentity;
					 }
					 completion:completion];
}



#pragma mark - KVC
- (void)setPrev:(PPTSRowView *)aRow
{
	if (aRow != prev) {
		PPTSRowView *oldPrev = prev;
		prev = aRow;
		prev.next = self;
		oldPrev.next = nil;
	}
}

- (void)setNext:(PPTSRowView *)aRow
{
	if (aRow != next) {
		PPTSRowView *oldNext = next;
		next = aRow;
		next.prev = self;
		oldNext.prev = nil;
	}
}



#pragma mark - KVC
- (void)setActive:(BOOL)flag
{
	if (flag != active) {
		active = flag;
		//self.backgroundColor = active ? [UIColor colorWithWhite:0.8f alpha:1.f] : [UIColor whiteColor];
	}
}



#pragma mark - Utilities
/**
 *	Given a focus Y returns our corresponding y position, calculated from our targetY
 *	@attention targetY must have been set when calling this method
 */
- (CGFloat)yForFocusY:(CGFloat)focusY inBubble:(CGFloat)bubbleSize withDensity:(CGFloat)density
{
	CGFloat expFraction = [self fractionForFocusY:focusY withDensity:density];
	
	// calculate position
	CGFloat distance = 0.5f * bubbleSize - (expFraction * bubbleSize);
	
	focusY += distance;
	return roundf(focusY);
}

/**
 *	Returns the transformation suitable for the given focus y position. Do NOT use the transformed focusY!
 *	@attention targetY must have been set when calling this method
 */
- (CATransform3D)transformForFocusY:(CGFloat)focusY inBubble:(CGFloat)bubbleSize withDensity:(CGFloat)density
{
	CGFloat expFraction = [self fractionForFocusY:focusY withDensity:density];
	CGFloat myHeight = [self bounds].size.height;
	
	// calculate fold amount (smooth between 0.1 and 0.9)
	CGFloat foldAmount = 0.9f;
	foldAmount -= expFraction * 0.8f;
	
	// create a transformation
	CATransform3D t = CATransform3DIdentity;
	t = CATransform3DMakeTranslation(0.f, 0.f, myHeight/2);
	t.m34 = 1.f / (-3 * myHeight);
	t = CATransform3DRotate(t, -M_PI_2 * foldAmount, 1.f, 0.f, 0.f);
	CGFloat scale = 0.85f + (expFraction * 0.11f);
	t = CATransform3DScale(t, scale, scale, 1.f);
	
	return t;
}

/**
 *	Returns an exponential fraction between 0 and 1, with 0 for the rows farthest down from focus and 1 farthest up from focus
 *	@attention targetY must have been set when calling this method
 */
- (CGFloat)fractionForFocusY:(CGFloat)focusY withDensity:(CGFloat)density
{
	NSUInteger num = 10;
	CGFloat distIndex = (targetY - focusY) / density;
	CGFloat fraction = fminf(1.f, fmaxf(0.f, (num/2 - distIndex) / num));
	
	return exponentialEaseInOut(fraction);
}



#pragma mark - Drawing
- (void)drawRect:(CGRect)rect
{
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextSaveGState(ctx);
	
	CGRect myBounds = [self bounds];
	
	// fill with border color
	CGContextSetFillColorWithColor(ctx, [[UIColor lightGrayColor] CGColor]);
	CGContextFillRect(ctx, myBounds);
	
	// fill with white to leave left, top and right border
	CGRect fill = CGRectInset(myBounds, borderWidth, borderWidth);
	fill.size.height += borderWidth;
	CGContextSetFillColorWithColor(ctx, [self.backgroundColor CGColor]);
	CGContextFillRect(ctx, fill);
	
	// add text
	if ([text length] > 0) {
		CGContextSetFillColorWithColor(ctx, [[UIColor blackColor] CGColor]);
		if (textLocation.size.width < 1.f || textLocation.size.height < 1.f) {
			textLocation = CGRectInset([self bounds], 10.f, 10.f);
		}
		[text drawInRect:textLocation withFont:font];
	}
	
	CGContextRestoreGState(ctx);
}


@end


CGFloat exponentialEaseInOut(CGFloat t)
{
	t = fminf(fmaxf(t, 0.f), 1.f);
	CGFloat strength = 5.f;
	
	t *= 2.f;
	if (t < 1.f) {
		return 1.f / 2.f * powf(2.f, strength * (t - 1.f) );
	}
	t--;
	return 1.f / 2.f * ( -powf(2.f, -strength * t) + 2.f );
}

