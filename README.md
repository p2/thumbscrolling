Thumb Scrolling
===============

A simulation of flipping through cards with your thumb, implemented as a subclass of `UITableViewController`, utilizing the standard delegate and dataSource
methods.

I started playing with this when I thought it might be a faster way to scroll to a distant part in the table view than using the side index, but it isn't. At
least it is kinda neat, so if anybody wants to edge out the remaining issues and use it, please do so! It's Apache licensed, so you can pretty much do with it
what you want.


Requirements
------------

 - iOS 4 (project uses ARC)


Demo App
--------

The included demo data are the 1000 most popular US names as found on http://www.ssa.gov/oact/babynames/. The demo App is based on the default table view app
template that you get when Xcode creates a fresh project. Take a look at MasterViewController to see what's going on compared to a standard
UITableViewController.